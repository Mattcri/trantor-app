from dataclasses import fields
from pyexpat import model
from rest_framework import serializers
from .models import Credential, Book, Editorial, Gender, Author

class CredentialSerializers(serializers.ModelSerializer):
  class Meta:
    model = Credential
    fields = '__all__'

class EditorialSerializer(serializers.ModelSerializer):
  class Meta:
    model = Editorial
    fields = ['name',]

class AuthorSerializer(serializers.ModelSerializer):
  class Meta:
    model = Author
    fields = ['first_name', 'last_name']

class BookSerializer(serializers.ModelSerializer):
  editorial = EditorialSerializer(many=False, read_only=True)
  genders = serializers.StringRelatedField(many=True, read_only=True)
  author = AuthorSerializer(many=True, read_only=True)
  # cover_image = serializers.SerializerMethodField()
  class Meta:
    model = Book
    fields = '__all__'