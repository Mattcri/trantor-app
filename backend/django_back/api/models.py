from django.db import models

# Create your models here.

class Credential(models.Model):
  username = models.CharField(max_length=45)
  password = models.CharField(max_length=30)

  class Meta:
    db_table = 'credential'

  def __str__(self):
    return self.username

# Note: Para agregar FK a modelos debo tener instanciado el modelo. EJ si ponemos abajo la class 
# editorial de book obtendremos que el modelo Editorial no ha sido definido, otra solución es poner
# '' el modelo que se quiere pasar como parametro para una FK o una conexión de ManyFields
class Editorial(models.Model):
  name = models.CharField(max_length=120)
  class Meta:
    db_table = 'editorial'
  def __str__(self):
    return self.name

class Gender(models.Model):
  gender_name = models.CharField(max_length=120)
  class Meta:
    db_table = 'gender'
  def __str__(self):
    return self.gender_name

class Author(models.Model):
  first_name = models.CharField(max_length=100)
  last_name = models.CharField(max_length=100)
  class Meta:
    db_table = 'author'
  def __str__(self):
    return self.first_name + ' ' + self.last_name

class Book (models.Model):
  title = models.CharField(max_length=80)
  edition = models.CharField(max_length=50)
  comentary = models.TextField(max_length=200)
  cover_image = models.ImageField(null=True, blank=True)
  editorial = models.ForeignKey(Editorial, on_delete= models.SET_NULL, null=True)
  genders = models.ManyToManyField(Gender, blank=True)
  author = models.ManyToManyField(Author, blank=True, null=True)

  class Meta:
    db_table = 'book'

  def __str__(self):
    return self.title
