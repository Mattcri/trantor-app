from django.urls import path
from . import views

urlpatterns = [
  path('', views.overview, name="overview"),
  path('credentials/', views.credentialList, name="credentials"),
  path('books/', views.booksList, name="books-list"),
  path('books/<str:pk>', views.getBook, name="book-detail"),
  path('books-create/', views.postBook, name="book-create"),
  path('upload-file/', views.uploadImage, name="upload image")
]
