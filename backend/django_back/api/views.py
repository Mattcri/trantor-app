from email.policy import default
from multiprocessing import context
from django.shortcuts import render
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import CredentialSerializers, BookSerializer
from .models import Credential, Book

# Create your views here.
@api_view(["GET"])
def overview(request):
  api_urls = {
    'credentials': '/credentials/',
    'books': '/books/',
    'book': '/books/id',
    'book-create': '/book-create/',
    'book-delete': '/book-delete/',
    'book-update': '/book-update/',
    'users': '/users/token/',
  }
  return Response(api_urls)

@api_view(["GET"])
def credentialList(request):
  credentials = Credential.objects.all()
  serializers = CredentialSerializers(credentials, many=True)
  return Response (serializers.data)

@api_view(['GET'])
def booksList(request):
  context = {'request': request}
  books = Book.objects.all()
  serializer = BookSerializer(books, many=True, context=context)
  return Response(serializer.data)

@api_view(['GET'])
def getBook(request, pk):
  context = {'request': request}
  book = Book.objects.get(id=pk)
  serializer = BookSerializer(book, many=False, context=context)
  return Response(serializer.data)

@api_view(['POST'])
def postBook(request):
  serializer = BookSerializer(data=request.data)
  if serializer.is_valid():
    serializer.save()
  return Response(serializer.data)

@api_view(['POST'])
def uploadImage(request):
  file = request.data.get['file', None]
  if file:
    return Response({"message": "File upload succesfuly"}, status=200)
  else:
    return Response({"message": "File upload fail"}, status=400)



# NOTE_IMPORTANT = al intentar traer la imágen puede que haya estado instanciando mal el contexto
# por lo que fue necesario poder poner la variable context y luego pasarla como párametro en la
# respuesta dedel serializer