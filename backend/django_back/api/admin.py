from django.contrib import admin
from .models import Credential, Book, Editorial, Gender, Author

# Register your models here.
admin.site.register(Credential)
admin.site.register(Book)
admin.site.register(Editorial)
admin.site.register(Gender)
admin.site.register(Author)