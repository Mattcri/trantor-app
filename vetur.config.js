module.exports = {
  // **optional** default: `{}`
  // override vscode settings part
  // Notice: It only affects the settings used by Vetur.
  settings: {
    "vetur.useWorkspaceDependencies": true,
    "vetur.experimental.templateInterpolationService": false
  },
  // **optional** default: `[{ root: './' }]`
  // support monorepos
  projects: [
    './frontend', // shorthand for only root.
    {
      root: './frontend',
      package: './package.json',
      // It is relative to root property.
      globalComponents: [
        './src/components/**/*.vue'
      ]
    }
  ]
}