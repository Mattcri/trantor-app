import { createStore } from 'vuex'

const API = 'http://localhost:8000/api/'

export default createStore({
  state: {
    authors: null,
    books: null,
    book: null,
  },
  getters: {
  },
  mutations: {
    GET_AUTHORS (state, authors) {
      state.authors = authors
    },
    GET_BOOKSLIST (state, books) {
      state.books = books
    },
    GET_BOOKDETAIL (state, book) {
      state.book = book
    }
  },
  actions: {
    async getBooksList({ commit }) {
      try {
        let api = await fetch(`${API}books/`)
        let response = await api.json()
        let data = commit('GET_BOOKSLIST', response)
        return data

      } catch (err) {
        console.error(err)
      }
      // return fetch(`${API}books/`)
      //   .then(responseApi => responseApi.json())
      //   .then(data => {
      //     console.log(data)
      //     commit('GET_BOOKSLIST', data)
      //   })
      //   .catch(reject => console.error(reject))
    },
    getNamesAuthors({ commit }) {
      return fetch(`${API}books/`)
        .then(res => res.json())
        .then(data => {
          let authorField = data.map(data => data.author[0])
          commit('GET_AUTHORS', authorField)
        })
        
        .catch(rej => console.error(rej))
    }

  },
  modules: {
  }
})
