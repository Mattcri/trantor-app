# Trantor App

Proyecto para el curso; Taller de Desarrollo de Aplicaciones

## Backend

Tecnologías ocupadas para el backend:
- Django
- Django REST Framework para la API
- BBDD MySQL

## Frontend

Tecnologías utilizadas para la construcción del frontend:
- Vue JS
- Tailwind CSS
